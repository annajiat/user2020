+++
title = "Participation"
description = "Volunteering, Speaking and Attending"
keywords = ["tickets","call for papers","volunteer","organiser"]
id = "register"
+++

<br>
### <i class="fas fa-user-plus"></i> Registration
Registration is [now open](/registration/instructions/)! Diversity Scholarship applicants should wait to register until after the announcement of DS awards (around Feb 15) because registration fees will be waived for awardees.

<br>
### <i class="fa fa-credit-card"></i> Cancellation Policy
Registrations canceled before June 1, 2020 receive a 50% refund. Registrations canceled after June 1, 2020 are not eligible for a refund. If you are denied a visa and you applied for your visa before May 1, 2020, we will refund 100% of your registration fees even after the cancellation deadline. Applying for a visa after May 1, 2020 and not getting an answer does not qualify you for a late refund. Refund requests must be received no later than July 1, 2020, with accompanying documents showing a visa denial. Contact us to apply for a refund.

<br>
### <i class="fa fa-glass"></i> R-Ladies Social Event
[R-ladies] (https://rladies.org/) is a worldwide organization whose mission is to promote gender diversity in the R community, in response to the underrepresentation of minority genders (including but not limited to cis/trans women, trans men, non-binary, genderqueer, agender). If you identify as one of these minorities, please join us for an R-Ladies reception during Thursday night of useR! 2020. Come interact with other R-minority users, meet some of the R-Ladies global team, hear about existing chapters and events close to your hometown, or learn how to set up your local R-ladies chapter.

<br>
### <i class="fa fa-graduation-cap" aria-hidden="true"></i> Mentoring 
Attendees looking to connect with and learn from an established R user in their field have the opportunity to be matched with a mentor at the useR! 2020 conference. Any attendee is welcome to sign up to be a mentee or a mentor when registering for the conference. Mentor/mentee pairs will be announced prior to the conference via email and will be formally introduced during a meeting the morning of the first day of the conference (Wednesday, July 8th, 2020). This opportunity provides participants the chance to learn from their peers and form professional connections. Mentees will be given a mentor on a first-come, first-served basis depending on the number of mentors who sign up. 

<br>
### <i class="fas fa-handshake"></i> Job Fair
The event will be more laid-back than a traditional career fair, so that prospective employees and employers have the opportunity to learn about each other in a more relaxed setting. The space will be set up with tables around the perimeter of the room, on which companies can display marketing material and/or pamphlets. The center of the room will be open with cocktail tables that can be used for prospective employees and employers to mingle as they please. The event will occur right around happy hour, so alcoholic beverages will be provided as well

<br>
### <i class="fas fa-tshirt"></i> Conference T-shirt
There will be a conference T-shirt, design is currently ongoing. A draft can be [found here](/registration/tshirt/). Registrants will have the opportunity to decline a t-shirt during registration if they desire.

<br>
### <i class="fas fa-users"></i> Diversity Scholarships
As with past useR! conferences, we will be accepting applications for diversity scholarships, which will partially subsidize travel expenses and provide a full registration ticket (including two tutorials and the gala dinner events). There will also be a dedicated networking event for diversity scholarship recipients with the conference's sponsors.The application submission page is closed, winners have been contacted via email.

<br>
### <i class="fas fa-chalkboard-teacher"></i> Call for Tutorial Proposals
The call for proposals has closed, all applicants have been notified of their acceptance. A preliminary schedule can be found at the [Tutorial](/program/tutorials/) page.

<br>
### <i class="fas fa-comment"></i> Call for Papers
The call for papers has closed. We anticipate announcing acceptances on March 16th, 2020.

