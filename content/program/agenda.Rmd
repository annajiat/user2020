---
title: Agenda
output: 
  md_document:
    preserve_yaml: true
---

```{r setup, echo=FALSE, message=FALSE, warning=FALSE}
library(googlesheets4)
library(lubridate)
library(tidyverse)
library(stringr)
library(RCurl)
library(knitr)
library(kableExtra)
library(pander)
gs4_deauth()

schedule <- read_sheet("https://docs.google.com/spreadsheets/d/1ZbqTod0inlJ9niUUTn4gxWlAf3WXACdpzpCV5EtzHLI/", col_types = "c")

titleday <- stamp("Sunday, July 1, 2020")
titletime <- stamp_time("14:00 UTC")
timestring <- stamp_time("14:00")

new_schedule <- schedule %>% 
  filter(`Time confirmed` == "yes") %>% 
  select(date = `Date (Day.Month)`,
         Time_UTC,
         Duration,
         Title,
         Type,
         YouTube,
         speakers = `Speaker(s)`) %>% 
  mutate(datetime = str_c(date,2020," ",Time_UTC)) %>% 
  mutate(datetime = dmy_hm(datetime)) %>% 
  mutate(Title = replace_na(Title, "TBD")) %>% 
  mutate(YouTube = replace_na(YouTube, '/404/')) %>% 
  mutate(speakers = replace_na(speakers, "TBD")) %>%
  mutate(time_url = str_c("https://arewemeetingyet.com/UTC/",
                          date(datetime),
                          "/",
                          timestring(datetime),
                          "/",
                          curlEscape(Title))) %>% 
  mutate(Title = paste0('<a href="', YouTube, '">', Title, '</a>')) %>% 
  mutate(Time = paste0('<a href="', time_url, '">', titletime(datetime), '</a>')) %>% 
  arrange(datetime)

```

## useR! 2020 is now a virtual conference.  

The conference will be free and online. Talks will be available on [YouTube](https://www.youtube.com/channel/UC_R5smHVXRYGhZYDJsnXTwg) either live or pre-recorded. All YouTube content will be captioned.

There is no need to sign up or register for the talks. Just come by!

A calendar of events can be found at the bottom of the page. You can also subscribe to the calendars via [iCal](https://calendar.google.com/calendar/ical/a38nnigbugd5bebvvdvsi60jbo%40group.calendar.google.com/public/basic.ics) or [Google](https://calendar.google.com/calendar/embed?src=a38nnigbugd5bebvvdvsi60jbo%40group.calendar.google.com&ctz=Europe%2FBerlin)

Questions for all keynote talks are being hosted on [slido](https://app.sli.do/event/ccilmxcz/live/questions).

Live captions for all keynotes can be found at [streamtext](https://www.streamtext.net/player?event=useR2020).

### Breakout Sessions

```{r breakout, echo=FALSE, message=FALSE, results='asis'}
breakout <- new_schedule %>% 
  filter(Type == "breakout") %>% 
  mutate(Day = titleday(datetime)) %>% 
  select(Day, Time, Duration, Speakers = speakers, Title) %>% 
  kable(escape= FALSE, col.names = c("Date", "Time", "Duration", "Speakers", "Title (with video link)")) %>%
  kable_styling(full_width = T) %>%
  column_spec(1, width = "20%") %>%
  column_spec(2, width = "10%") %>%
  column_spec(3, width = "10%") %>%
  column_spec(4, width = "30%") %>%
  column_spec(5, width = "30%")
  print(breakout)
```
### Welcome Session

```{r welcome, results='asis', echo=FALSE, message=FALSE}
welcome <- new_schedule %>% 
  filter(Type == "Welcome") %>% 
  mutate(Day = titleday(datetime)) %>% 
  select(Day, Time, Duration, Speakers = speakers, Title) %>% 
  kable(escape=FALSE, col.names = c("Date", "Time", "Duration", "Speakers", "Title (with video link)"))  %>% 
  kable_styling(full_width = T) %>%
  column_spec(1, width = "20%") %>%
  column_spec(2, width = "10%") %>% 
  column_spec(3, width = "10%") %>% 
  column_spec(4, width = "30%") %>% 
  column_spec(5, width = "30%")
  print(welcome)
```

### Keynote Sessions

```{r keynote, echo=FALSE, message=FALSE, results='asis'}
keynote <- new_schedule %>% 
  filter(Type == "keynote") %>% 
  mutate(Day = titleday(datetime)) %>%
  select(Day, Time, Duration, Speakers = speakers, Title) %>% 
  kable(escape=FALSE, col.names = c("Date", "Time", "Duration", "Speakers", "Title (with video link)"))  %>% 
  kable_styling(full_width = T) %>%
  column_spec(1, width = "20%") %>%
  column_spec(2, width = "10%") %>% 
  column_spec(3, width = "10%") %>% 
  column_spec(4, width = "30%") %>% 
  column_spec(5, width = "30%")
  print(keynote)
```

### R Core Event

```{r core, echo=FALSE, message=FALSE, results='asis'}
core <- new_schedule %>% 
  filter(Type == "R core event") %>% 
  mutate(Day = titleday(datetime)) %>%
  select(Day, Time, Duration, Speakers = speakers, Title) %>% 
  kable(escape=FALSE, col.names = c("Date", "Time", "Duration", "Speakers", "Title (with video link)"))  %>% 
  kable_styling(full_width = T) %>%
  column_spec(1, width = "20%") %>%
  column_spec(2, width = "10%") %>% 
  column_spec(3, width = "10%") %>% 
  column_spec(4, width = "30%") %>% 
  column_spec(5, width = "30%")
  print(core)
```

<iframe src="https://calendar.google.com/calendar/embed?src=a38nnigbugd5bebvvdvsi60jbo%40group.calendar.google.com&ctz=Europe%2FBerlin" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>