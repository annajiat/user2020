---
title: Contributed Sessions
output: 
  md_document:
    preserve_yaml: true
aliases:
  - /program/posters/
---
  
```{r setup, echo=FALSE, message=FALSE}
library(googlesheets4)
library(dplyr)
library(knitr)
library(kableExtra)
library(pander)
gs4_deauth()
videos <- read_sheet("https://docs.google.com/spreadsheets/d/1m8b6GuVgMUop4HV8X3N6TS3ODluN0pp4ngV6jd9RbAs/edit#gid=0", col_types = "c")
```

useR! 2020 contributions were accepted for one of three formats:

- **regular**: ~15 minute talk
- **lightning**: ~5 minute talk
- **poster**: ~3 minute talk, with option to submit poster as supplementary material

These contributions have been arranged into topic-based sessions and corresponding video playlists are available on [YouTube](https://www.youtube.com/channel/UC_R5smHVXRYGhZYDJsnXTwg/playlists). Session titles link to their specific playlists.  

```{r tables, results='asis', echo=FALSE}
session_urls <- as.list(c('https://www.youtube.com/playlist?list=PL4IzsxWztPdnA1fTRNOuZ5X6h6JIEBDAk',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdkA184_5Ag0lJOouJoh1CQ4',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdk3pyH1I7s1M-AIcJAl-L9x',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdn6cCCtkMOaBqwtQDBqhOvV',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdlCNDOcOk6AEf6U0fPFz9gU',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdmZTCHo_wWVpuC6pn8zgZN3',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdmhinFbZCevaqkDeCFyvSNY',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdmqml-u7PvYOVLdSvD7kjby',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdl-nV0mlFzj9eegXX12Oz9S',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdnYnNGCxzcUvgLxKCGbbt9Y',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdnpKAKicyTzyvExVi3FvO56',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdm7kXX8z6P0fBzcHYHMtY2O',
                         'https://www.youtube.com/playlist?list=PL4IzsxWztPdl7hYfuzLdRaBuf59HCycvn'))

names(session_urls) <- as.list(sort(unique(videos$Session)))

for (sessions in as.list(sort(unique(videos$Session)))) {
  cat(paste('## [', sessions, '](', session_urls[sessions], ')\n'))
  tmp_video <- videos %>% 
    filter(Session == sessions) %>% 
    mutate(`Video Type` = factor(`Video Type`, 
                                 levels = c("regular", "lightning", "poster")),
           YouTube = paste0('<a href="', `Youtube link`, '">video</a>'),
           Materials = paste0('<a href="', `Zip Folder Link`, '">files</a>')) %>% 
    select(Title,
           `Speaker(s)` = Speaker,
           `Talk Type` = `Video Type`,
           YouTube,
           Materials
    ) %>% 
    arrange(`Talk Type`, Title) %>% 
    kable(escape = FALSE) %>%
    kable_styling(full_width = T) %>%
    column_spec(1, width = "40%") %>%
    column_spec(2, width = "20%") %>%
    column_spec(3, width = "10%") %>%
    column_spec(4, width = "15%") %>% 
    column_spec(5, width = "15%")  
  print(tmp_video)
}
```