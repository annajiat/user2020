---
title: Contributed Sessions
output: 
  md_document:
    preserve_yaml: true
aliases:
  - /program/posters/
---

useR! 2020 contributions were accepted for one of three formats:

-   **regular**: ~15 minute talk
-   **lightning**: ~5 minute talk
-   **poster**: ~3 minute talk, with option to submit poster as
    supplementary material

These contributions have been arranged into topic-based sessions and
corresponding video playlists are available on
[YouTube](https://www.youtube.com/channel/UC_R5smHVXRYGhZYDJsnXTwg/playlists).
Session titles link to their specific playlists.

[Biostatistics](https://www.youtube.com/playlist?list=PL4IzsxWztPdnA1fTRNOuZ5X6h6JIEBDAk)
-----------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
Mortality Minder
</td>
<td style="text-align:left;width: 20%; ">
Karan Bhanot
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/iDeXLL8-co4">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1aT-s4P8x8-VUVXmQAVkJPZzCIkgSPUz4/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
pepFunk: peptide-centric functional enrichment for metaproteomic gut
microbiome data
</td>
<td style="text-align:left;width: 20%; ">
Caitlin Simopoulos
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/UZtpi-Bg9i0">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1ypNP1Oy7ssDYFv0qrFrcAP0i033e43G_/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
rejustify AI-enhanced ETL engine
</td>
<td style="text-align:left;width: 20%; ">
Marcin Wolski
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/ML8drcG_1xg">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1748C5K3vObP1W4NwjtoV20wyN-5bcjGd/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Building descriptive tables with the updated compareGroups package
</td>
<td style="text-align:left;width: 20%; ">
Isaac Subirana
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/BIQxUO39Erg">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/16JXm97r7q-zps0Rit4VhO9C_zfp0-yvl/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
MeningiOMICS
</td>
<td style="text-align:left;width: 20%; ">
Kaitlyn O’Shea
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/zjaJRw7Atus">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1uxjbYhgjFg1PFFxx7qnlWoOx9P0UkMAP/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Methods of report generation in clinical trials using R and knitr
</td>
<td style="text-align:left;width: 20%; ">
Alessio Maggiorelli
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/zbhRkb9UP7U">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/17cI2a60AqJZUH5XsUnByeSifeYsn-Nhy/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Robust alternatives for comparing time-event survival curves with R
</td>
<td style="text-align:left;width: 20%; ">
Lubomir Stepanek
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/y6nVmbrsrJw">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1_fsa2H1GcQOZT8ZuaylwrXUsQboZ2dP_/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Comparing the Treatment Regimen of Newly Diagnosed Pediatric Leukemia
Patients
</td>
<td style="text-align:left;width: 20%; ">
Sierra Davis
</td>
<td style="text-align:left;width: 10%; ">
poster
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/qToKh1qev2c">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1svK2zrU2LZDS29Lq7Xi1ty3Yv3U6ACZE/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
MUACz: An R Package for Generating MUAC and BMI z-scores and Percentiles
for Children and Adolescents
</td>
<td style="text-align:left;width: 20%; ">
Lazarus Mramba
</td>
<td style="text-align:left;width: 10%; ">
poster
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/tGx2Apq3VWw">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1VwEp4bxBnVdx33JsFAHhgP4OvA9HL4Ot/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Community](https://www.youtube.com/playlist?list=PL4IzsxWztPdkA184_5Ag0lJOouJoh1CQ4)
-------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
Communities of practice in Latin America: R and Friends
</td>
<td style="text-align:left;width: 20%; ">
Laura Acion et al
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/gtRntU2J3Cg">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1WeEs23nj58RRgEibaeNAtwKED2LaAxm2/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
R community explorer: the state of R
</td>
<td style="text-align:left;width: 20%; ">
Rick Pack (Ben Ubah)
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/7F7iQdcV8RY">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/106pZRYg-zHYWnjshI-ptQzywuNZxAUaP/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Tidy Tuesday: Scaffolding for a Community of Practice
</td>
<td style="text-align:left;width: 20%; ">
Tom Mock
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/H8rhQOj7vDY">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/18HcY2qeijfVv-gb2-sTeZCa3QKFUZIey/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Improving open data accessibility through package development and
community work
</td>
<td style="text-align:left;width: 20%; ">
Diego Kozlowski
</td>
<td style="text-align:left;width: 10%; ">
poster
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/CuyuIS8LZhE">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1g4Jok6MHNQ7Ae3JRk3ohIzQBpcLi9nMj/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Data Management](https://www.youtube.com/playlist?list=PL4IzsxWztPdk3pyH1I7s1M-AIcJAl-L9x)
-------------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
DataSailr package enables intuitive row by row data manipulation
</td>
<td style="text-align:left;width: 20%; ">
Toshihiro Umehara
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/5KnLebiKYuA">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1FnKgllj1yQoXBUuCjrqrsqIs5hnzCRo6/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Larger-than-memory R
</td>
<td style="text-align:left;width: 20%; ">
Konrad Siek
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/6BI5xWWDrlA">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1C782g11whwyqbwqvDaZFo9iMt8528TpX/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
PostgreSQL As A Data Science Database
</td>
<td style="text-align:left;width: 20%; ">
Parfait Gasana
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/69tVWeibs_U">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1YQyFtzefqViMJv89_EjHmUnwB8XTXdLu/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
staypuft: object validation and serialization
</td>
<td style="text-align:left;width: 20%; ">
Scott Chamberlain
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/bqoxhSy7hlg">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1myMdJd9Gn34CFPyb_8zcVCL1BpwDvv2_/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
themis: dealing with imbalanced data by using synthetic oversampling
</td>
<td style="text-align:left;width: 20%; ">
Emil Hvitfeldt
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/kL5qWitjvNg">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1ga28QS-QU4Rym5w9gYu0XtIeEMvi7iY2/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
missMethods - An R Package for Creating and Handling Missing Data as
well as Evaluating Missing Data Methods
</td>
<td style="text-align:left;width: 20%; ">
Tobias Rockel
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/G6JCXaPjG9g">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1oWvVdfUHgLBHbvnmIezgn7jPS0LVW6Rs/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
SynthTools: An R Package to Measure Synthetic Data Utility
</td>
<td style="text-align:left;width: 20%; ">
Charlotte Looby
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/SZK7Y_4p_TM">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1MdZFqbF64cQ4tDULO26nXM7K3a4FzOXr/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Economics and Finance](https://www.youtube.com/playlist?list=PL4IzsxWztPdn6cCCtkMOaBqwtQDBqhOvV)
-------------------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
BVAR: Bayesian Vector Autoregressions with Hierarchical Prior Selection
in R
</td>
<td style="text-align:left;width: 20%; ">
Nikolas Kuschnig
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/kos9yzhvL1I">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1DocYNHbCunnwCK0kkKSTEnqx2mo_Xbk6/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
R for Data Privacy and Governance
</td>
<td style="text-align:left;width: 20%; ">
Alyssa Columbus
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/_hflzfcdQ4A">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1cN2ICxK0DybeSudP3vWKPhtwCzuGNcUQ/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
svars: An R Package for Data-Driven Identification in Multivariate Time
Series Analysis
</td>
<td style="text-align:left;width: 20%; ">
Alexander Lange
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/3e1qAtQJLqo">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1TrZ48ZtK9EOEsJyr2Lg-zo0pRdbhtgY9/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Understanding Machine Learning for Diversified Portfolio Construction by
Explainable AI
</td>
<td style="text-align:left;width: 20%; ">
Dimitri Marinelli
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/lrADaEAek9Y">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1tektFt13A2CLyvMIBmcWbBqzlg6_bCzG/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Multi-stage Financial Modeling using Machine Learning in R
</td>
<td style="text-align:left;width: 20%; ">
Yong Chen, xuanyan cai
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/-cgwDHzd0p4">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1JGTlOf4USPm6Byszi71RScqaLFwDPFY8/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
How green is your portfolio? Tracking CO2 footprint in the insurance
sector
</td>
<td style="text-align:left;width: 20%; ">
Antoine Logean
</td>
<td style="text-align:left;width: 10%; ">
poster
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/uwxQgGcKvmo">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1NlKj9mYrRKYpNi2wgCs3I89580vJvCZO/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Machine Learning and Statistics](https://www.youtube.com/playlist?list=PL4IzsxWztPdlCNDOcOk6AEf6U0fPFz9gU)
-----------------------------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
Fast.ai in R preserving wildlife with computer vision
</td>
<td style="text-align:left;width: 20%; ">
Jedrzej Swiezewski
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/6nryqkGtkbw">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="nothing%20submitted">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
KeOps: Seamless Kernel Operations on GPU without memory overflows
</td>
<td style="text-align:left;width: 20%; ">
B. Charlier, G. Durif
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/5DDd_88fJH0">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1WfuypnWU9yS3zHXRyJqgsqzaykYTRN9P/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Extending flexmix to model-based clustering with sparse data
</td>
<td style="text-align:left;width: 20%; ">
Bettina Grün
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/pwzCoHl8QMQ">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1O0sNbYqNofBCN-f0UcQcID9mWzum5H4T/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
How to simplify Machine Learning workflows specifications?
</td>
<td style="text-align:left;width: 20%; ">
Anton Antonov
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/b9Uu7gRF5KY">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1k9MYSCUllY1OcxYEz4ZM567w0udIBG-m/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
mlr3hyperband: Multi-Fidelity Hyperparameter Optimization with R
</td>
<td style="text-align:left;width: 20%; ">
Sebastian Gruber
</td>
<td style="text-align:left;width: 10%; ">
poster
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/mB7M6LWIeKM">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1Bn6YVOBbW3crZwn2QqejOtvPuSIk5kAJ/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Towards Trustworthy Machine Learning: Debugging with Expert Knowledge
</td>
<td style="text-align:left;width: 20%; ">
Helena Kotthaus
</td>
<td style="text-align:left;width: 10%; ">
poster
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/7JHRkL5921g">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1smSXg34N3GjSD5NUFYsJbe7IdeC_j4Ja/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Programming](https://www.youtube.com/playlist?list=PL4IzsxWztPdmZTCHo_wWVpuC6pn8zgZN3)
---------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
A method for deriving information from running R code
</td>
<td style="text-align:left;width: 20%; ">
Mark van der Loo
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/55sP4ytE1uc">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1gJnqWVemtLjwoJjCc9s2kAD2bhWKEZwe/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
A Risk-based Assessment for R Package Accuracy
</td>
<td style="text-align:left;width: 20%; ">
Andy Nicholls, Juliane Manitz
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/WUVUjdqifJ8">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1KNZBDlLiK8oQZSPizxJTftZOr9zGsXo1/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Arbitrary precision linear algebra in R using RcppEigen and BH
</td>
<td style="text-align:left;width: 20%; ">
Max Turgeon
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/4jarvGZ9s9k">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1mhaSxY9QxIYlvG7nMfcuvW8td0gUb3cK/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Flexibility in routines by using s4 classes: calculating habitat
conservation status as an example
</td>
<td style="text-align:left;width: 20%; ">
Els Lommelen
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/hdsHbVDviYA">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1-C2wBnjIzvBME0-XyYzkpPylqx3L99GG/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
How fastR can help R package developers
</td>
<td style="text-align:left;width: 20%; ">
Stepan Sindelar
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/xtHOI4M2KXk">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/12nT6JXH9QjuT5iinh781kgqy_LmZ4fjP/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
The Current State and Future Prospects of Encoding Support in R
</td>
<td style="text-align:left;width: 20%; ">
Tomas Kalibera
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/J9lraMP1ono">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1RXkenqvYfAvuL5hZR7GMnErmzmkEBu8J/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
FasteR code: vectorizing computations in R
</td>
<td style="text-align:left;width: 20%; ">
S Gwynn Sturdevant
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/Rmc4uSpKLWA">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1FCLoysF4yuprmj__BRMIafpBS21FvYtL/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Why continuous integration will save your team and your packages
</td>
<td style="text-align:left;width: 20%; ">
Sebastian Engel-Wolf
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/lP0FjbNvz-Q">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1wdBE_B6vsCJZCnSce2xsnW4h8qT5IIrh/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
LabVIEW-R interface
</td>
<td style="text-align:left;width: 20%; ">
damjan krizmancic
</td>
<td style="text-align:left;width: 10%; ">
poster
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/3pCipET5jRg">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1F-Kj1plVz0OgELkK6l9qPCvWT5Ew9CZ-/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Reproducible Research](https://www.youtube.com/playlist?list=PL4IzsxWztPdmhinFbZCevaqkDeCFyvSNY)
-------------------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
Be proud of your code!
</td>
<td style="text-align:left;width: 20%; ">
Marcin Dubel
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/xrALIrUEshc">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="nothing%20submitted">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Rapid & Reproducible Resistance Analysis For All - The AMR package for R
</td>
<td style="text-align:left;width: 20%; ">
Matthijs Berends
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/uqaKwk-VAdA">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1R_SNJaD7QJlqNFQIeLJlNHoXS4lSTypD/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Using Pins to Ensure Reproducibility with Datasets
</td>
<td style="text-align:left;width: 20%; ">
Javier Luraschi
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/8dDG2ULde8M">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1_f3k5VMBiWGVlV-dCEKxZno3WaqC8qPL/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
PRDA package: Enhancing Statistical Inference via Prospective and
Retrospective Design Analysis.
</td>
<td style="text-align:left;width: 20%; ">
Claudio Zandonella Callegher
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/IkCzuJei3_E">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/14dZZeqYc9pFOd2CXs6O3lvvYP8hUAxZA/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
projmgr: Managing the human dependencies of your project
</td>
<td style="text-align:left;width: 20%; ">
Emily Riederer
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/8OfaIOICD20">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1FtE9GXw_UF2RJg0hIfAjYRQ8OVuaOKS1/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Reactor: reactive notebooks for R
</td>
<td style="text-align:left;width: 20%; ">
Herb Susmann
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/hytdjtM6Chg">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1UAPV0YbiplRsnda7bwhR3j0vUN9ygb7D/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Why Should I use the Here Package When I’m Already Using Projects
</td>
<td style="text-align:left;width: 20%; ">
Malcolm Barrett
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/QYrdsjBvZN4">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1YUJXCtESntzxSWC9QNCaiDESjLpxhX2v/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Social Science](https://www.youtube.com/playlist?list=PL4IzsxWztPdmqml-u7PvYOVLdSvD7kjby)
------------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
blavaan: An R package for Bayesian structural equation modeling
</td>
<td style="text-align:left;width: 20%; ">
Ed Merkle
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/zWA43-6taNA">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1CxiRLUvu8TJUEX8DXi5NBlFf8ZMM7PSz/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
sensemakr Sensitivity Analysis Tools for OLS
</td>
<td style="text-align:left;width: 20%; ">
Carlos Cinelli
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/p3dfHj6ki68">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1u9w2spcq6BIlXdwwrdu-DMCrcUkbU3Sv/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
simpr: concise and readable simulations for the tidyverse
</td>
<td style="text-align:left;width: 20%; ">
Ethan Brown
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/MkfMSe9re2U">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1THcInWrneSg18W_wRBYMXr-gODTzQjIt/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Social Categorizations in CRAN data
</td>
<td style="text-align:left;width: 20%; ">
Simon Couch
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/TRaY_Fk-26g">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/18FvIHnj3COyw-wswgUrWRs0PhC949-xJ/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Why Social Workers Need Data Science
</td>
<td style="text-align:left;width: 20%; ">
Gina Griffin
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/orLyTf5fk4U">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1uoxgrpTUp28_92rfTsaOaWtegqrELomk/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Word Embeddings and Geometric Deep Learning for Social Network Analysis
</td>
<td style="text-align:left;width: 20%; ">
Michael Ghisletti
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/fQatBdVIzOI">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1dyiXm3jqVkGIHsaH8GqqtPA014v1pdAJ/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Scoring the implicit: The implicitMeasures package
</td>
<td style="text-align:left;width: 20%; ">
Ottavia Epifania
</td>
<td style="text-align:left;width: 10%; ">
poster
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/INa426Ru40Y">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1thvgGPjiA7jh0gpiMwOyoHwxDAL5AF0i/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Spatial](https://www.youtube.com/playlist?list=PL4IzsxWztPdl-nV0mlFzj9eegXX12Oz9S)
-----------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
arcgisbinding: An R package for integrating R in ArcGIS
</td>
<td style="text-align:left;width: 20%; ">
Orhun Aydin
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/T8ZzJzp0b34">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1aSCO-BGFfOYRhWHHvpriizj3TQAqgpqE/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
bcdata: Reproducible spatial data retrieval from a Geoserver Web Service
</td>
<td style="text-align:left;width: 20%; ">
Andy Teucher
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/ZNRouw-BmzE">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/19Px1HGXfFOIeRGkaBeV__VcIXXx_pekt/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Mapping 311 Requests by Community Area in Chicago Using Shiny, Leaflet,
and The Tidyverse
</td>
<td style="text-align:left;width: 20%; ">
Jim Kloet
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/vsjbcKsORGs">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1CR9bYEDzFYOXw_L7L-1o8c2qjV-eubtD/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Privacy protected maps using adaptive kernel density estimation
</td>
<td style="text-align:left;width: 20%; ">
Edwin de Jonge
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/M10SmUs68x4">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/177IsBtxzDGxdsfypdNGzlzHy73q5qKlq/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Teaching](https://www.youtube.com/playlist?list=PL4IzsxWztPdnYnNGCxzcUvgLxKCGbbt9Y)
------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
Learning R Through Spaced Repetition and the Remembr Package
</td>
<td style="text-align:left;width: 20%; ">
Daniel Jacobs
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/zgApxtOLMX8">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1GJq0K2B8smwQLzEsQkVOROlXgxouMC4z/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Moving backwards with R
</td>
<td style="text-align:left;width: 20%; ">
Aimee Schwab-McCoy
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/hPQwGyc9SGA">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1kn0yYor5c9Leto11hCURTYSUuPJ2gCBs/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Teaching R with Peer Review and a New Rubric
</td>
<td style="text-align:left;width: 20%; ">
Alon Friedman, Zachariah Beasley
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/x8l9DGx4SKc">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1b-tn5WCnM8SymMxaJhKrAJIiLPXxq0_o/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Framing transparency as an ethical responsibility in PhD data science
</td>
<td style="text-align:left;width: 20%; ">
Casey Canfield
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/YMqsIWu_7ZA">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1lng8xdBkVDKXD_gzJzt980_ps9XtvRNF/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
University Libraries for R: Supporting R useR!s
</td>
<td style="text-align:left;width: 20%; ">
Maze Ndukum, Dorris Scott, Marcy Vana
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/THOaiWGW7rw">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1AlZB8QPplXWjqrgQGJXXmQaw_Ndv8yGQ/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Time Series](https://www.youtube.com/playlist?list=PL4IzsxWztPdnpKAKicyTzyvExVi3FvO56)
---------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
LDATS: Latent Dirichlet Allocation Coupled with Time Series Analyses
</td>
<td style="text-align:left;width: 20%; ">
Juniper Simonis
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/ezjOj8lPLfg">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1Jd7nNUBTEKlnBv016iF1rBFptnokH7Ws/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
muHVT: Case based computational geometry modeling toolkit using R
</td>
<td style="text-align:left;width: 20%; ">
Zubin Dowlaty
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/K-eEfnWBE8s">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1dv-ISDAvW94GL0ESHtK9J9ja4ojYkrub/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
An online scheduling approach for distributed additive manufacturing
systems
</td>
<td style="text-align:left;width: 20%; ">
Ícaro Agostino
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/8wXRir49b9w">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1VtMJT7nUeqfQJxHHER6y00TdRoxY5IOH/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
ztsdb, a time-series DBMS for R users
</td>
<td style="text-align:left;width: 20%; ">
Leonardo Silvestri
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/duXYx_8TYm0">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1BUJLs4XbKHvaMsOD9R-STTitFmVeyawK/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Visualization](https://www.youtube.com/playlist?list=PL4IzsxWztPdm7kXX8z6P0fBzcHYHMtY2O)
-----------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
Doing Journalism with R
</td>
<td style="text-align:left;width: 20%; ">
Benedict Witzenberger
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/1bmdHy5vtfY">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1smt-pL2CajLb9YjWiT766ninqHA7WFTs/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
rtables
</td>
<td style="text-align:left;width: 20%; ">
Gabriel Becker
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/CBQzZ8ZhXLA">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/11-YIM8yXTYtnj19uWuBNXw_1pXytJzlZ/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Visualization of missing data and imputations in time series
</td>
<td style="text-align:left;width: 20%; ">
Steffen Moritz
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/KZgvYBedGYo">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1IEIGsoSFHRu7LDta-XhJ0MNoZrWz1R6x/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Your slides are so extra!
</td>
<td style="text-align:left;width: 20%; ">
Gerrick Aden-Buie
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/vZMuu77ocMY">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1y-aFDggcYgzAf8dS1ffj2nhBIuQPybgv/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Improving accessibility in data visualizations created by ggplot2
</td>
<td style="text-align:left;width: 20%; ">
Christine Chai
</td>
<td style="text-align:left;width: 10%; ">
poster
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/mbi_JVC1arM">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1SkERvYn85pdoNHF0262C6oxU-6WA1Btu/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>

[Web Applications](https://www.youtube.com/playlist?list=PL4IzsxWztPdl7hYfuzLdRaBuf59HCycvn)
--------------------------------------------------------------------------------------------

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Title
</th>
<th style="text-align:left;">
Speaker(s)
</th>
<th style="text-align:left;">
Talk Type
</th>
<th style="text-align:left;">
YouTube
</th>
<th style="text-align:left;">
Materials
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 40%; ">
Manage your Shiny app users
</td>
<td style="text-align:left;width: 20%; ">
Alexander Matrunich
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/_-DkR0ZjXMI">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1JZEqJ_TAz73fB-6GB_Z_Qyzn3CZN7RCX/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
shi18ny: Internationalization for Shiny Apps
</td>
<td style="text-align:left;width: 20%; ">
Juan Pablo Marin Diaz
</td>
<td style="text-align:left;width: 10%; ">
regular
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/IVSCQHcTFLQ">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1A-3ETa_9ptXcvWnscnqBOALbkkqZMuX5/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Cybersecurity risk assessment with R
</td>
<td style="text-align:left;width: 20%; ">
Corey Neskey
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/i0fIRhrSAzE">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1UaxUrUqx6AwtqzxrNU7tSiYacR8r5M-x/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
metashiny: build Shiny apps with a Shiny app
</td>
<td style="text-align:left;width: 20%; ">
Bo Wang
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/0hmQer9l4eY">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1PBmve5Fv8zXyTxmzxj8XFSYVTocqvhOp/view?usp=sharing">files</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 40%; ">
Very Easy Web Scraping with ralger
</td>
<td style="text-align:left;width: 20%; ">
Mohamed El Fodil Ihaddaden
</td>
<td style="text-align:left;width: 10%; ">
lightning
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://youtu.be/OHi6E8jegQg">video</a>
</td>
<td style="text-align:left;width: 15%; ">
<a href="https://drive.google.com/file/d/1C0X8ZtCz5nivF7zV9wcqI8LzCwg0H62x/view?usp=sharing">files</a>
</td>
</tr>
</tbody>
</table>
