---
title: Tutorials
output: 
  md_document:
    preserve_yaml: true

---

Online Tutorials
----------------

<table class="table" style="width: auto !important; margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Date
</th>
<th style="text-align:left;">
Time
</th>
<th style="text-align:left;">
Duration
</th>
<th style="text-align:left;">
Speakers
</th>
<th style="text-align:left;">
Title/Registration link (when available)
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 20%; ">
Tuesday, July 14, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-14/15:00/Seamless%20R%20and%20C%2B%2B%20integration%20with%20Rcpp">15:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
02:00
</td>
<td style="text-align:left;width: 30%; ">
Dirk Eddelbuettel
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://t.co/nfN1V7Aaqa?amp=1">Seamless R and C++ integration
with Rcpp</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Tuesday, July 21, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-21/18:00/Creating%20beautiful%20data%20visualizations%20in%20R%3A%20a%20ggplot2%20crash%20course">18:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
02:00
</td>
<td style="text-align:left;width: 30%; ">
Sam Tyner
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://www.eventbrite.com/e/creating-beautiful-data-visualizations-in-r-a-ggplot2-crash-course-tickets-112524126692">Creating
beautiful data visualizations in R: a ggplot2 crash course</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Thursday, July 23, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-23/01:30/Easy%20Larger-than-RAM%20data%20manipulation%20with%20disk.frame">01:30
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
02:00
</td>
<td style="text-align:left;width: 30%; ">
ZJ Dai
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://www.meetup.com/es-ES/rladies-san-diego/events/271785235/">Easy
Larger-than-RAM data manipulation with disk.frame</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Thursday, July 23, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-23/15:30/Create%20and%20share%20reproducible%20code%20with%20R%20Markdown%20and%20workflowr">15:30
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
02:00
</td>
<td style="text-align:left;width: 30%; ">
John Blischak, Peter Carbonetto
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://bit.ly/3fDdj4Z">Create and share reproducible code with
R Markdown and workflowr</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Friday, July 24, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-24/20:00/Predictive%20modeling%20with%20text%20using%20tidy%20data%20principles">20:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
01:30
</td>
<td style="text-align:left;width: 30%; ">
Julia Silge, Emil Hvitfeldt
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://github.com/RLadiesEnArgentina/user2020tutorial">Predictive
modeling with text using tidy data principles</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Monday, July 27, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-27/14:00/First%20steps%20in%20spatial%20data%20handling%20and%20visualization">14:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
03:00
</td>
<td style="text-align:left;width: 30%; ">
Sebastien Rochette, Dorris Scott, Jakub Nowosad
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://www.eventbrite.co.uk/e/first-steps-in-spatial-data-handling-and-visualization-tickets-113141537384">First
steps in spatial data handling and visualization</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Wednesday, July 29, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-29/16:00/Causal%20inference%20in%20R">16:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
02:00
</td>
<td style="text-align:left;width: 30%; ">
Lucy D’Agostino McGowan, Malcom Barrett
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://www.meetup.com/rladies-la/events/271909700/">Causal
inference in R</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Friday, July 31, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-31/13:00/Getting%20the%20most%20out%20of%20Git">13:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
02:30
</td>
<td style="text-align:left;width: 30%; ">
Colin Gillespie, Rhian Davies
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://www.meetup.com/rladies-east-lansing/events/271969821/">Getting
the most out of Git</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Tuesday, August 04, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-08-04/15:00/Reproducible%20computation%20at%20scale%20with%20drake%3A%20hands-on%20practice%20with%20a%20machine%20learning%20project">15:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
04:00
</td>
<td style="text-align:left;width: 30%; ">
Will Landau
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://www.meetup.com/rladies-cambridge/events/271866242/?isFirstPublish=true">Reproducible
computation at scale with drake: hands-on practice with a machine
learning project</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Friday, August 07, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-08-07/15:00/Machine%20Learning%20with%20mlr3">15:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
03:00
</td>
<td style="text-align:left;width: 30%; ">
Bernd Bischl, Michel Lang
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://www.meetup.com/es/rladies-galapagos-islands/events/272240650/">Machine
Learning with mlr3</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Friday, August 07, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-08-07/16:00/End-to-end%20machine%20learning%20with%20Metaflow%3A%20Going%20from%20prototype%20to%20production%20with%20Netflix%E2%80%99s%20open%20source%20project%20for%20reproducible%20data%20science">16:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
02:30
</td>
<td style="text-align:left;width: 30%; ">
Savin Goyal, Bryan Galvin, Jason Ge
</td>
<td style="text-align:left;width: 30%; ">
<a href="http://metaflowr.eventbrite.com">End-to-end machine learning
with Metaflow: Going from prototype to production with Netflix’s open
source project for reproducible data science</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Saturday, August 15, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-08-15/16:00/Periscope%20and%20CanvasXpress%20%E2%80%93%20Creating%20an%20enterprise-grade%20big-data%20visualization%20application%20in%20a%20day">16:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
03:30
</td>
<td style="text-align:left;width: 30%; ">
Connie Brett
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://www.eventbrite.com/e/periscope-and-canvasxpress-creating-an-enterprise-big-data-visualization-tickets-113198228950">Periscope
and CanvasXpress – Creating an enterprise-grade big-data visualization
application in a day</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Saturday, September 19, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-09-19/16:00/Analyzing%20and%20visualising%20spatial%20and%20spatiotemporal%20data%20cubes">16:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
03:00
</td>
<td style="text-align:left;width: 30%; ">
Edzer Pebesma, Martijn Tennekes
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://www.meetup.com/rladies-monterrey/events/272174013/?isFirstPublish=true">Analyzing
and visualising spatial and spatiotemporal data cubes</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Monday, September 21, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-09-21/15:00/Application%20of%20Gaussian%20graphical%20models%20to%20metabolomics">15:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
02:00
</td>
<td style="text-align:left;width: 30%; ">
Denise Scholtens, Raji Balasubramanian
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://docs.google.com/forms/d/1xs2UEUvTnKCRvYZ70aUw5GPEy7ePn5jDqFqNI-_Y1IQ/viewform?edit_requested=true">Application
of Gaussian graphical models to metabolomics</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
TBD
</td>
<td style="text-align:left;width: 10%; ">
TBD
</td>
<td style="text-align:left;width: 10%; ">
NA
</td>
<td style="text-align:left;width: 30%; ">
Shirin Elsinghorst
</td>
<td style="text-align:left;width: 30%; ">
Deep Learning with Keras and TensorFlow
</td>
</tr>
</tbody>
</table>
