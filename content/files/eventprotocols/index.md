# **useR! 2020 - Live Event Protocols**


## **Workflow**

Live events will be hosted using Zoom, which will then be streamed live to YouTube with a 20 second delay built in. Q and A will be hosted using Sli.do ("Slido"), which will open at the beginning of each live event and remain open until its conclusion. The R Consortium’s YouTube channel will be used for these events.

## **General Considerations**

Prior to your live event, please:

1. Ensure that you have an internet connection that meets [Zoom’s requirements](https://support.zoom.us/hc/en-us/articles/201362023-System-requirements-for-Windows-macOS-and-Linux#h_d278c327-e03d-4896-b19a-96a8f3c0c69c). Please let the organizing team know if you have any concerns about your bandwidth access.

2. Make sure you are comfortable sharing your screen if you are a keynote speaker.

3. Select a space for streaming that does not have a distracting background (white is preferable). 

4. **Please make sure you enter talks with your video and microphone turned off - especially if you are participating in a panel. Our technical team will be using the waiting room feature in Zoom and will remind folks of this.**

## **Roles and Expectations**

### *Speakers*

Please keep single keynote talks to approximately 45-50 minutes, and remember that your head will be in the upper right corner on top of your slides. For the joint R-Core keynotes, please keep your talks to 25-30 minutes each.

Please join Zoom 15 minutes before the start of your talk.

### *Session Chair*

These individuals will serve as the chair and discussant for each keynote speaker session. They should plan to introduce themselves quickly, **thank RStudio for sponsoring useR! 2020**, and then provide a quick introduction of the speaker. Chairs will be provided with basic biographies, but should feel free to adapt them as needed. They should have their cameras turned on for this portion of the program.

Once chairs turn the session over to the speaker, they can feel free to turn their camera off until the Q and A session begins. Once Q and A begins, Chairs should read questions out loud to the speaker and then give them time to answer the question. There are two options:

1. the Chair can monitor the Slido room themselves, or

2. they can ask the technical team member to message questions, one at a time, to the chair via Zoom’s chat.

Chairs should generally follow the Slido questions - answering those with the most up-votes first. If the question is duplicative of one that has already been answered, Chairs should feel free to skip it. Chairs can also exercise discretion if it seems that a related question has not received as many up votes but provides a natural extension of the discussion or highlights a particularly important point or contribution.

At the end, they should thank the audience.

### *Technical Team Member*

The technical team members are Chris and Heidi. They will be responsible for setting-up and managing the zoom session. This includes:

1. Sending out a zoom invitation to all participants ahead of time

2. Starting the zoom session ~20 minutes before the official start time

3. Assign Amanda as the captioner once she joins the room

4. Posting an initial placeholder graphic and then starting YouTube streaming

5. Monitor the YouTube stream to ensure it is working properly

6. Make sure the chair and speaker are spotlighted as needed

7. Monitor Slido and moderate as needed

More info on YouTube + Zoom can be found in the [documentation provided by the *Why R?* team]() (thank you so much!).



**Technical Checklist**

* Slido:

    * Prepare slido session. Only one session (the upcoming one) should be active at any time.

    * Make sure that the right stream is embedded

        * On YouTube go to the stream -> share button/arrow (top right) -> embed (in German "einbetten") -> copy embedding code ![image alt text](image_0.png)

        * On slido  go to Event settings -> Integrations -> Live video -> … -> edit links -> paste embedding code ![image alt text](image_1.png)

    * Add link to stream description [https://app.sli.do/event/ccilmxcz](https://app.sli.do/event/ccilmxcz) 

* Captioning:

* Make sure that captioner is available, has been assigned as captioner and that captions work

* Visibility:

    * Tell everyone: Turn off your camera and mute if you don't want to be seen (e.g. chair during talk or technical person during entire event)

* In case of switches between speakers:

    * Prepare a slide to show during the switch with the necessary information (ask someone else to share it)

* Before starting the stream:

    * Tell people that you will be starting the stream now and they should be prepared for starting. Say "we are live" once YouTube says that stream has started.

